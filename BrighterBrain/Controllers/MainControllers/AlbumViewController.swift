//
//  AlbumViewController.swift
//  BrighterBrain
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import UIKit

class AlbumViewController: ResultViewController {

    override var cellClass: BaseCell.Type {
        return AlbumCell.self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

         NotificationCenter.default.addObserver(self,selector: #selector(reloadViewData),name: Notification.dataReset,object: nil)
    }
    
    @objc func reloadViewData(notification: NSNotification){
        if let searchString = notification.object as? String {
            currentSearchText = searchString
            currentPage = 1
            self.getAlbums(text: searchString)
        }
    }

    
    override func requestData(completion: @escaping (Bool, [JsonMappable]) -> Void) {
        getAlbums(text: currentSearchText)
    }
    
    func getAlbums(text:String){
        AlbumStore.shared.getAlbums(search: text,page:currentPage) { (success, albums:[Album]) in
            if success{
                if self.currentPage == 1 {
                    self.itemsArray.removeAll()
                }
                self.itemsArray.append(contentsOf: albums)
            }
        }
    }

    override func didSelect(indexPath: IndexPath) {
        guard let album = itemsArray[indexPath.row] as? Album else{return}
        getAlbumInfo(mbid: album.mbid ?? "")
    }

    
    func getAlbumInfo(mbid:String){
        AlbumStore.shared.getInfo(mbid: mbid) { (success, album:Album?) in
            if success{
                let controller = UIStoryboard.getViewControllerGeneric(type: AlbumDetailController.self)
                controller.model = album
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
}
