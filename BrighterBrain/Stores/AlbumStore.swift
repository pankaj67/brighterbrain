//
//  AlbumStore.swift
//  BrighterBrain
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import UIKit
class AlbumStore: ServiceManager {
    
    static let shared = AlbumStore()
    
    func getAlbums<T:JsonMappable>(search:String,page:Int,completion:@escaping (_ success:Bool,_ list:[T]) -> Void){
        let albumSearchURL = Constants.ALBUM_SEARCH + "&album=\(search)" + "&page=\(page)"
        requestAPI1(albumSearchURL) { (resultDict,success) in
            guard let result = resultDict else { completion(false, []);return}
            let baseResponse = BaseApiResponseAlbum.init(fromJson: JSON.init(result["results"] as Any))
            completion(true,baseResponse.albummatches?.album as? [T] ?? [])
        }
    }
    
    func getInfo<T:JsonMappable>(mbid:String,completion:@escaping (_ success:Bool,_ list:T?) -> Void){
        let albumSearchURL = Constants.ALBUM_INFO + "&mbid=\(mbid)"
        requestAPI1(albumSearchURL) { (resultDict,success) in
            guard let result = resultDict else { completion(false,nil);return}
            let baseResponse = T.init(fromJson: JSON.init(result["album"] as Any))
            completion(true,baseResponse)
        }
    }
}


