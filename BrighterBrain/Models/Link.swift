//
//	Link.swift
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import Foundation 


class Link : NSObject, NSCoding,JsonMappable{

	var text : String!
	var href : String!
	var rel : String!
	var link : Link!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    required init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		text = json["#text"].stringValue
		href = json["href"].stringValue
		rel = json["rel"].stringValue
		let linkJson = json["link"]
		if !linkJson.isEmpty{
			link = Link(fromJson: linkJson)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if text != nil{
			dictionary["#text"] = text
		}
		if href != nil{
			dictionary["href"] = href
		}
		if rel != nil{
			dictionary["rel"] = rel
		}
		if link != nil{
			dictionary["link"] = link.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         text = aDecoder.decodeObject(forKey: "#text") as? String
         href = aDecoder.decodeObject(forKey: "href") as? String
         rel = aDecoder.decodeObject(forKey: "rel") as? String
         link = aDecoder.decodeObject(forKey: "link") as? Link

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if text != nil{
			aCoder.encode(text, forKey: "#text")
		}
		if href != nil{
			aCoder.encode(href, forKey: "href")
		}
		if rel != nil{
			aCoder.encode(rel, forKey: "rel")
		}
		if link != nil{
			aCoder.encode(link, forKey: "link")
		}

	}

}
