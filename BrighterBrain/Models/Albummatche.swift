//
//	Albummatche.swift
//  BrighterBrain
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//
import Foundation 

class Albummatche : NSObject, NSCoding,JsonMappable{

	var album : [Album]!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    required init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		album = [Album]()
		let albumArray = json["album"].arrayValue
		for albumJson in albumArray{
			let value = Album(fromJson: albumJson)
			album.append(value)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if album != nil{
			var dictionaryElements = [[String:Any]]()
			for albumElement in album {
				dictionaryElements.append(albumElement.toDictionary())
			}
			dictionary["album"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         album = aDecoder.decodeObject(forKey: "album") as? [Album]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if album != nil{
			aCoder.encode(album, forKey: "album")
		}

	}

}
