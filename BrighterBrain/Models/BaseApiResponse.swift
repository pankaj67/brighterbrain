//
//	BaseApiResponse.swift
//  BrighterBrain
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//
import Foundation

protocol JsonMappable {
    init(fromJson json: JSON!)
}

class BaseApiResponseTrack : BaseApiResponse{
    
    var trackmatches : Trackmatche!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    required init(fromJson json: JSON!){
        super.init(fromJson: json)
        if json.isEmpty{
            return
        }
        let trackmatchesJson = json["trackmatches"]
        if !trackmatchesJson.isEmpty{
            trackmatches = Trackmatche(fromJson: trackmatchesJson)
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    override func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if trackmatches != nil{
            dictionary["trackmatches"] = trackmatches.toDictionary()
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
         trackmatches = aDecoder.decodeObject(forKey: "trackmatches") as? Trackmatche
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    override func encode(with aCoder: NSCoder)
    {
        if trackmatches != nil{
            aCoder.encode(trackmatches, forKey: "trackmatches")
        }

    }
    
    
}


class BaseApiResponseArtist : BaseApiResponse{
    
    var artistmatches : Artistmatche!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    required init(fromJson json: JSON!){
        super.init(fromJson: json)
        if json.isEmpty{
            return
        }
        let artistmatchesJson = json["artistmatches"]
        if !artistmatchesJson.isEmpty{
            artistmatches = Artistmatche(fromJson: artistmatchesJson)
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    override func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if artistmatches != nil{
            dictionary["artistmatches"] = artistmatches.toDictionary()
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
         artistmatches = aDecoder.decodeObject(forKey: "artistmatches") as? Artistmatche
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    override func encode(with aCoder: NSCoder)
    {
        if artistmatches != nil{
            aCoder.encode(artistmatches, forKey: "artistmatches")
        }
    }
    
    
}


class BaseApiResponseAlbum : BaseApiResponse{
    
    var albummatches : Albummatche!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    required init(fromJson json: JSON!){
          super.init(fromJson: json)
        if json.isEmpty{
            return
        }
        let albummatchesJson = json["albummatches"]
        if !albummatchesJson.isEmpty{
            albummatches = Albummatche(fromJson: albummatchesJson)
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    override func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if albummatches != nil{
            dictionary["albummatches"] = albummatches.toDictionary()
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        albummatches = aDecoder.decodeObject(forKey: "albummatches") as? Albummatche
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    override func encode(with aCoder: NSCoder)
    {
        if albummatches != nil{
            aCoder.encode(albummatches, forKey: "albummatches")
        }
    }


}

class BaseApiResponse : NSObject, NSCoding,JsonMappable{

	var opensearchitemsPerPage : String!
	var opensearchstartIndex : String!
	var opensearchtotalResults : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    required init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		opensearchitemsPerPage = json["opensearch:itemsPerPage"].stringValue
		opensearchstartIndex = json["opensearch:startIndex"].stringValue
		opensearchtotalResults = json["opensearch:totalResults"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		
		
		if opensearchitemsPerPage != nil{
			dictionary["opensearch:itemsPerPage"] = opensearchitemsPerPage
		}
		if opensearchstartIndex != nil{
			dictionary["opensearch:startIndex"] = opensearchstartIndex
		}
		if opensearchtotalResults != nil{
			dictionary["opensearch:totalResults"] = opensearchtotalResults
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         opensearchitemsPerPage = aDecoder.decodeObject(forKey: "opensearch:itemsPerPage") as? String
         opensearchstartIndex = aDecoder.decodeObject(forKey: "opensearch:startIndex") as? String
         opensearchtotalResults = aDecoder.decodeObject(forKey: "opensearch:totalResults") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{

		if opensearchitemsPerPage != nil{
			aCoder.encode(opensearchitemsPerPage, forKey: "opensearch:itemsPerPage")
		}
		if opensearchstartIndex != nil{
			aCoder.encode(opensearchstartIndex, forKey: "opensearch:startIndex")
		}
		if opensearchtotalResults != nil{
			aCoder.encode(opensearchtotalResults, forKey: "opensearch:totalResults")
		}

	}

}
