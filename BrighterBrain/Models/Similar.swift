//
//	Similar.swift
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import Foundation 

class Similar : NSObject, NSCoding,JsonMappable{

	var artist : [Artist]!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    required init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		artist = [Artist]()
		let artistArray = json["artist"].arrayValue
		for artistJson in artistArray{
			let value = Artist(fromJson: artistJson)
			artist.append(value)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if artist != nil{
			var dictionaryElements = [[String:Any]]()
			for artistElement in artist {
				dictionaryElements.append(artistElement.toDictionary())
			}
			dictionary["artist"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         artist = aDecoder.decodeObject(forKey: "artist") as? [Artist]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if artist != nil{
			aCoder.encode(artist, forKey: "artist")
		}

	}

}
