//
//	Toptag.swift
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//


import Foundation 

class Toptag : NSObject, NSCoding,JsonMappable{

	var tag : [Tag]!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    required init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		tag = [Tag]()
		let tagArray = json["tag"].arrayValue
		for tagJson in tagArray{
			let value = Tag(fromJson: tagJson)
			tag.append(value)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if tag != nil{
			var dictionaryElements = [[String:Any]]()
			for tagElement in tag {
				dictionaryElements.append(tagElement.toDictionary())
			}
			dictionary["tag"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         tag = aDecoder.decodeObject(forKey: "tag") as? [Tag]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if tag != nil{
			aCoder.encode(tag, forKey: "tag")
		}

	}

}
