//
//  ArtworkCell.swift
//  BrighterBrain
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import UIKit

class ArtworkCell: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
    var model : Image? {
        didSet{
            guard let model = model else {
                return
            }
            imgView.load.request(with: model.text)
        }
    }
    var artist : Artist? {
        didSet{
            guard let artist = artist else {
                return
            }
            if let images = artist.image , images.count > 0 {
                imgView.load.request(with: images[0].text)
            }
        }
    }
    
}
