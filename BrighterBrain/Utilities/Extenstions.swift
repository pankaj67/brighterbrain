//
//  Extenstions.swift
//  BrighterBrain
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    class var main: UIStoryboard {
        let storyboardName: String = Bundle.main.object(forInfoDictionaryKey: "UIMainStoryboardFile") as! String
        return UIStoryboard(name: storyboardName, bundle: nil)
    }
    
    class var login:UIStoryboard{
        let storyboardName: String = "Login"
        return UIStoryboard(name: storyboardName, bundle: nil)
    }
    
    class func getViewController(type:UIViewController.Type) -> UIViewController{
        return main.instantiateViewController(withIdentifier: String(describing:type.self))
    }
    
    class func getViewControllerGeneric<T>(type:T.Type) -> T{
        return main.instantiateViewController(withIdentifier: String(describing:type.self)) as! T
    }
    
    class func getViewController(type:UIViewController.Type,storyboard:UIStoryboard) -> UIViewController{
        return login.instantiateViewController(withIdentifier: String(describing:type.self))
    }
    
    class func getVCGeneric<T,S:UIStoryboard>(type:T.Type,storyboard:S) -> T?{
        return storyboard.instantiateViewController(withIdentifier: String(describing:type.self)) as? T
    }
}

extension Notification {
    static let dataReset = Notification.Name(rawValue: "dataReset")
}


class AlertFactory{
    
    
    static func showAlert(title:String? = nil,message:String,completion:( (_ result: Bool) -> Void)? = nil) {
        AlertFactory.showAlert(title: title ?? "", message: message, cancelTitle: "", okTitle: "Ok", completion: completion)
    }
    
    
  private  static func showAlert(title:String,message:String,cancelTitle:String,okTitle:String,completion:( (_ result: Bool) -> Void)?) {
        let _ :UIWindow = UIApplication.shared.keyWindow!
        let alertController = UIAlertController(title: message, message: nil, preferredStyle: UIAlertController.Style.alert)
        let ok = UIAlertAction(title: okTitle, style: .default, handler: { (action) -> Void in
            completion?(true)
        })
        if !cancelTitle.isEmpty {
            let cancel = UIAlertAction(title: cancelTitle, style: .default, handler: { (action) -> Void in
                completion?(false)
            })
            alertController.addAction(cancel)
        }
        alertController.addAction(ok)
        
        if let controller = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController {
            controller.present(alertController, animated: true, completion: nil)
        }
        else{
           UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
       // window.visibleViewController?.present(alertController, animated: true, completion: nil)
    }
    
}
